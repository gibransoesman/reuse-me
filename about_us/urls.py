from django.urls import path
from . import views

app_name = "about_us"

urlpatterns = [
    path('testimony-api/', views.testimony_api, name='testimony-api'),
    path('', views.about, name='about'),
]