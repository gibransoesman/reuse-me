from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import about_us
from .models import Aboutus


# Create your tests here.

class AboutUsPageUnitTest(TestCase):
    def test_about_page_url_is_exists(self):
        response = Client().get('/about_us/')
        self.assertEqual(response.status_code, 200)

    def test_about_page_using_about_function(self):
        found = resolve('/about_us/')
        self.assertEqual(found.func, about_us)

    def test_about_page_using_templates(self):
        response = Client().get('/about_us/')
        self.assertTemplateUsed(response, 'about.html')

    def test_cerita_model_can_add_new(self):
        Aboutus.objects.create(name='Test', text='test123')
        cerita_counts_all_entries = Aboutus.objects.all().count()
        self.assertEqual(cerita_counts_all_entries, 1)

    def test_self_func_name(self):
        Aboutus.objects.create(name='Test', text='test123')
        cerita = Aboutus.objects.get(name='Test')
        self.assertEqual(str(cerita), cerita.name)
