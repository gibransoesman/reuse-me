from django.urls import path
from . import views

app_name = 'login'
urlpatterns = [
	path('loged/', views.loged, name = 'loged'),
	path('', views.login, name = 'login'),
	path('logout/', views.logout, name = 'logout'),
]
