from django.shortcuts import render, redirect
from django.contrib.auth import logout as out

# Create your views here.
def login(request):
	return render(request, "login.html", {})

def loged(request):
	if (request.user.is_authenticated):
		request.session['user_email'] = request.user.email
		nama = request.user.first_name + " " + request.user.last_name
		nama = nama.upper()
		return render(request, "loged.html", {'nama' : nama})
	else:
		return redirect('login:login') 

def logout(request):
	request.session.flush()
	out(request)
	return redirect('login:login')